class ig.InfoBar
  (@parentElement, znacky) ->
    @znacky = znacky.map (name) -> {name}
    @element = @parentElement.append \div
      .attr \class \info-bar
    @heading = @element.append \h2
    @list = @element.append \ol
    @listItems = @list.selectAll \li .data @znacky .enter!append \li
      ..append \span
        ..attr \class \name
        ..html (.name)
    @listItemsProperties =
      values: @listItems.append \span .attr \class \value
      bars: @listItems.append \div .attr \class \bar

  drawList: ({properties}:feature) ->
    @heading.html properties.NAZOK
    for znacka in @znacky
      znacka.value = properties["znacka-okres_#{znacka.name}"]
    @znacky.sort (a, b) -> b.value - a.value
    for znacka, index in @znacky => znacka.index = index
    @listItems.style \top -> "#{it.index * 30}px"
    @listItemsProperties.values.html ({name}) ->
      absolute = properties["znacka-okres_#name"]
      relative = properties["perc-#name"]
      "#{ig.utils.formatNumber absolute} | #{ig.utils.formatNumber relative * 100, 2} %"
    @listItemsProperties.bars.style \width ({name}) ->
      "#{properties["perc-#name"] * 100}%"




