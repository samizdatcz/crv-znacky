container = d3.select ig.containers.base

mapContainer = container.append \div
  ..attr \class \map
map = ig.initMap mapContainer

geojson = topojson.feature ig.data.okresy, ig.data.okresy.objects."data"
znacky = <[AERO ALFA ARO AUDI BARKAS BMW CHEVROLET CHRYSLER CITROËN DACIA DAEWOO DAIHATSU DODGE FIAT FORD GM HONDA HYUNDAI JAGUAR JEEP KIA LADA LANCIA LAND LEXUS MAZDA MERCEDES MICRO MINI MITSUBISHI MOSKVIČ NISSAN OLTCIT OPEL PEUGEOT PORSCHE RENAULT ROVER SAAB SEAT SIMCA ŠKODA SMART SSANGYONG SUBARU SUZUKI TATRA TOYOTA TRABANT VAZ VOLHA VOLKSWAGEN VOLVO VOZIDLO WARTBURG ZASTAVA ŽUK]>
for feature in geojson.features
  feature.sum = 0
  feature.properties."znacka-okres_VOLKSWAGEN" += feature.properties."znacka-okres_VOLKSWAGEN_1"
  for znacka in znacky
    feature.sum += feature.properties."znacka-okres_#znacka"
  for znacka in znacky
    feature.properties."perc-#znacka" = feature.properties."znacka-okres_#znacka" / feature.sum
  feature.properties.znacky = for name in znacky
    absolute = feature.properties."perc-#name"
    relative = feature.properties."znacka-okres_#name"
    {name, absolute, relative}
  feature.properties.znacky.sort (a, b) -> b.absolute - a.absolute

lastLayer = null
ibOutline = null
infoBar = new ig.InfoBar container, znacky
draw = (znacka) ->
  style = if znacka != "Druhá značka"
    getPercentageStyle znacka
  else
    getSecondBrandStyle!
  if lastLayer
    map.removeLayer lastLayer

  lastLayer := L.geoJson do
    * geojson
    * style: style
      onEachFeature: (feature, layer) ->
        {properties} = feature
        absolute = properties["znacka-okres_#znacka"]
        relative = feature.properties["perc-#znacka"]
        layer.on \mouseover -> infoBar.drawList feature

  lastLayer.addTo map

getSecondBrandStyle = ->
  seconds =
    "VOLKSWAGEN" : \#1f78b4
    "FORD"       : \#33a02c
    "RENAULT"    : \#ffff99
    "PEUGEOT"    : \#6a3d9a
    "CITROËN"    : \#e31a1c
  return (feature) ->
    fillOpacity: 0.8
    weight: 0
    color: seconds[feature.properties.znacky[1].name]

getPercentageStyle = (znacka) ->
  extent = d3.extent geojson.features.map -> it.properties["perc-#znacka"]

  colors = ['rgb(237,248,251)','rgb(204,236,230)','rgb(153,216,201)','rgb(102,194,164)','rgb(44,162,95)','rgb(0,109,44)']

  projectedExtent = colors.map (d, i) ->
    extent.0 + (extent.1 - extent.0) * (i / (colors.length - 1))

  scale = d3.scale.linear!
    ..domain projectedExtent
    ..range colors

  return (feature) ->
    fillOpacity: 0.8
    weight: 0
    color: scale feature.properties["perc-#znacka"]

firstOption = "Druhá značka"
draw firstOption
infoBar.drawList geojson.features[0]
container.append \select
  .on \change -> draw @value
  .selectAll \option .data (["Druhá značka"] ++ znacky) .enter!append \option
    ..html -> it
    ..attr \selected -> it is firstOption || void

